<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HealthToolsFav extends Model
{
    protected $fillable = ['user_id','healthtool_slug','link','avatar','is_fav','org_image','new_link'];

    public function healthtool()
    {
        return $this->belongsTo('App\Healthtool','healthtool_slug');
    }
}
