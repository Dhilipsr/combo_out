<?php

namespace App;

use App\DepartmentsHealthtool;
use Illuminate\Database\Eloquent\Model;
use Auth;


class Healthtool extends Model
{
     public function department()
    {
        return $this->belongsToMany('App\Department');
    }

    public function scopeUserSpecific($query)
    {   
        if(Auth::user()->department_id != null && Auth::user()->has_department == 1){
            $query->wherein('id',DepartmentsHealthtool::where('department_id',Auth::user()->department_id)->pluck('healthtool_id'));
        }else{
           
        }
        
    }
    
    public function scopeTitleLike($query,$searchbar)
    {
        $query->where('title','like','%'.$searchbar.'%') ;
    }

    public function scopeActive($query)
    {
        $query->where(['status' => 1]) ;
    }

    public function scopeAscending($query)
    {
        $query->orderBy('title','asc') ;
    }

    public function scopeDescending($query)
    {
        $query->orderBy('title','DESC') ;
    }
}
