<?php 

namespace App\Http\Controllers;

use App\About;
use App\Contactenquiry;
use App\WrestlingMonster;
use App\Ehrplugin;
use App\Ehrstaticcontent;
use App\Evidencebasedworkflowscontent;
use App\Guideline;
use App\Guidelineandlinkscontent;
use App\HealthToolsFav;
use App\Healthtool;
use App\Healthtoolstaticcontent;
use App\HomePageContent;
use App\Homebannercontent;
use App\Homepagecontact;
use App\Patienttype;
use App\QualityCare;
use App\Reason;
use App\Service;
use App\Targetaudience;
use App\User;
use App\Workflow;
use App\DepartmentsHealthtool;
use Auth;
use DB;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;
use Illuminate\Validation\Rule;
use Mail;
use App\Http\Resources\UserHealthToolCollection;

class PagesController extends Controller
{   
    private $prettyLinkUrl;

    protected function setPrettyLinkUrl(string $link)
    {
        $prettyLinkUrl = $link;
    }

    protected function getPrettyLinkUrl(string $file,int $id)
    {
        $organization = Auth::user()->organization;
        // "PageTitle" => $healthtool->title,
        $curl = curl_init();
        $healthtool = Healthtool::find($id);

        $post = array(

                        "PageUrl" => $healthtool->api_id,
                        "OrganizationName" => $organization,
                        "Image" => array('$content-type'=> "image/jpeg",
                                                '$content'=> $file
                        ));
                        // return $file;
        curl_setopt_array($curl, array(
                                // CURLOPT_URL => 'https://cmsautomate.azure-api.net/cmsautomate-uat/getprettylink',
                                CURLOPT_URL => 'https://cmsautomate.azure-api.net/cmsautomate/getprettylink',
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_ENCODING => '',
                                CURLOPT_MAXREDIRS => 10,
                                CURLOPT_TIMEOUT => 0,
                                CURLOPT_FOLLOWLOCATION => true,
                                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                CURLOPT_CUSTOMREQUEST => 'POST',
                                CURLOPT_POSTFIELDS =>json_encode($post),
                                CURLOPT_HTTPHEADER => array(
                                    'Ocp-Apim-Subscription-Key: d186020dc07642acb4fc5493d05e4acb',
                                    'Content-Type: application/json'
                                ),
                                ));

        $response = curl_exec($curl);
        curl_close($curl);
        // return $response;
        $url = json_decode($response)->PrettyLinkUrl ;
        $this->setPrettyLinkUrl($url);

        return $url ;
    }
    // redirections
    public function index()
    {   
        $banner_contents = Homebannercontent::orderBy('sq_no')->get();
        $page_contents = HomePageContent::first();
        // print_r(json_encode($page_contents));
        // die;
        return view('welcome')->with(compact([
            'banner_contents',
            'page_contents',
        ]));
    }

    public function getService($slug)
    {
        $content = $this->serviceData($slug);
        $view = $this->servicePage($slug);
        // return json_encode($content);
        return view($view)->with(compact('content'));
    }


    public function healthtools()
    {   
        $healthtools = Healthtool::where(['status' => 1])->UserSpecific()->orderBy('title','ASC')->paginate(6);
        $healthtool_page_contents = Healthtoolstaticcontent::get();
        $targetaudiences = Targetaudience::where(['status' => 1])->get();
        $patienttypes = Patienttype::where(['status' => 1])->get();
        return view('healthtools')->with(compact([
            'healthtools',
            'targetaudiences',
            'patienttypes',
            'healthtool_page_contents'
        ]));
    }

  
    public function myaccount()
    {   
        $favTools = HealthToolsFav::where(['user_id' => Auth::user()->id,'is_fav' => 1 ])->get();
        return view('myaccount')->with(compact(['favTools']));
    }

    public function myaccount_update(Request $request)
    {   
        // return $request->all();
        
        $validatedData = $request->validate([
               'name' => ['required', 'string', 'max:255'],
               'lname' => ['required', 'string', 'max:255'],
               'organization' => ['required', 'string', 'max:255'],
               'contact' => ['required', 'max:12'],
               'update' => ['required'],
               'email' => ['required', 'string', 'email', 'max:255', Rule::unique('users')->ignore(Auth::id())],
           ]);

        $update_profile = User::find(Auth::id());
        $update_profile->name = $request->name;
        $update_profile->lname = $request->lname;
        $update_profile->organization = $request->organization;
        $update_profile->email = $request->email;
        $update_profile->contact = $request->contact;

        if (!empty($request->avatar)) {
            // change avatar
            if ($request->hasFile('avatar')) {
                $avatar = $request->file('avatar');
                
                $test_name = Rand(1,1000);

                $name = $test_name.'.'.$avatar->getClientOriginalExtension();
                $destinationPath = storage_path('/app/public/users');
                $avatarPath = $destinationPath. "/".  $name;
                $test =  $avatar->move($destinationPath, $name);
                //$grades->avatar = $name;
                $final_name = $name;
            }
            $update_profile->avatar = 'users/'.$final_name;
        }
        $check = $update_profile->save();

        if (!($check)) {
             
            return redirect()->back()->with(['message' => "Website Under maintenance, please try again later.!", 'alert-type' => 'error']);
        }else{
            
            return redirect()->back()->with(['message' => "Profile updated successfully.!", 'alert-type' => 'success']);
        }

    }
    public function checkpassword(Request $request)
    {
        $old_password = Hash::check($request->old_password, Auth::user()->password);

        if ($old_password == 1) {
            return response()->json(['status' => '1', 'message' => 'Correct Password.!']);
        }else{
            return response()->json(['status' => '0', 'message' => 'Wrong Password.!' ]);
        }

    }
    public function changepassword(Request $request)
    {   
        // validation for passwords
        $validatedData = $request->validate([
               'new_password' => ['required', 'min:8'],
           ]);

        $user = User::find(Auth::user()->id);
        if ($request->new_password == $request->password_confirmation) {
            $user->password = Hash::make($request->new_password);
            $update = $user->save();

              if (!$update) {
                return redirect()->back()->with(['message' =>'Your password has not been updated successfully.!', 'alert-type' => 'error']);
              }else{
                $name = Auth::user()->name;
                $email = Auth::user()->email;
               
                
                Mail::send('password_update_email',['email' => $email],function($message) use ($email){
                $message->subject('Password Changed');
                $message->from('noreply@LIVERHEALTHNOW.com','LIVERHEALTHNOW');
                $message->sender('noreply@LIVERHEALTHNOW.com', 'LIVERHEALTHNOW');
                $message->to($email);
                });
                
                return redirect()->back()->with(['message' => "Your password has been updated successfully.!", 'alert-type' => 'success']);
              }
        }else{
            return redirect()->back()->with(['message' => "Passwords do not match.!", 'alert-type' => 'error']);
        }
        
    }

    public function profilepicture(Request $request)
    {   
      if($_FILES["file"]["name"] != '')
      {
        $ext = $request->file->extension();
        $temp_name =  $request->file->getClientOriginalName();
        // $hashed = Hash::make($temp_name);
        $hashed = base64_encode($temp_name);
        $uid =  date('YmdHis');
        $new_name = $hashed.$uid;
        $final_name = $new_name.'.'.$ext;
        $save_image = $request->file->storeAs('public/users/',$final_name);
        if (!$save_image) {
          return response()->json(['message' => 2]);
        }else{
          // image saved

          // find his previous image and check if it is default.png
          $check_previous_image = $avatar = User::find(Auth::user()->id);
          if ($check_previous_image->avatar != "users/default.png") {
            $delete_old_profile_picture = unlink('storage/'.$check_previous_image->avatar);
           
          }
          // find his previous image and check if it is default.png
          $avatar = User::find(Auth::user()->id);
          $avatar->avatar = 'users/'.$final_name;
          $update = $avatar->save();
          if (!$update) {
            return response()->json(['message' => 0]);
          }else{
            return response()->json(['message' => 1]);
          }
        }
      }else{
        return response()->json(['message' => 3]);
      }
    }

    // functionality
    

    public function results()
    {
        return view('search');
    }
    
      public function userSpecific($healthtools)
    {
         if(Auth::user()->has_department == 1 && Auth::user()->department_id != null)
         {
             $final_tools = [];
                foreach($healthtools as $healthtool)
                {
                    if(in_array($healthtool->id,DepartmentsHealthtool::where('department_id',Auth::user()->department_id)->pluck('healthtool_id')->toArray()))
                    {
                        $final_tools[] = $healthtool->id ;

                    }
                }
                $healthtools = Healthtool::whereIn('id',$final_tools)->paginate(6);
                return $healthtools ;
        }else{
            return $healthtools;   
        }
    }

    public function healthtoolfilter(Request $request)
     {       
            $healthtool_page_contents = Healthtoolstaticcontent::get();
            $targetaudiences = Targetaudience::where(['status' => 1])->get();
            $patienttypes = Patienttype::where(['status' => 1])->get();
            // print_r($request->get('sort'));
            // die;
       if (empty($request->get('targetaudience')) && empty($request->get('patienttype')) ) {

            // if there is query
            if (!empty($request->get('searchbar'))) {

                if ($request->get('sort') == 'asc') {
                   $healthtools =  Healthtool::titleLike($request->get('searchbar'))
                                                ->active()
                                                ->ascending()
                                                ->userSpecific()
                                                ->paginate(6);

                }else if($request->get('sort') == 'desc'){
                    $healthtools = Healthtool::titleLike($request->get('searchbar'))
                                                ->active()
                                                ->descending()
                                                ->userSpecific()
                                                ->paginate(6);
                }

                $totalhealthtools =  Healthtool::titleLike($request->get('searchbar'))
                                                ->active()
                                                ->userSpecific()
                                                ->get();

            }else{
            // no query
                if ($request->get('sort') == 'asc') {
                    $healthtools = Healthtool::active()
                                                ->userSpecific()
                                                ->ascending()
                                                ->paginate(6);

                }else if($request->get('sort') == 'desc'){
                    $healthtools = Healthtool::active()
                                                ->userSpecific()
                                                ->descending()
                                                ->paginate(6);
                }

                $totalhealthtools =  Healthtool::userSpecific()->active()
                                                ->get();

            }

            return view('result')->with(compact([
                'totalhealthtools',
                'healthtools',
                'targetaudiences',
                'patienttypes',
                'healthtool_page_contents',
            ]));
       }else{
          
            $targetaudience_ids = ($request->get('targetaudience'));
            // print_r($targetaudience_id);
            // die;
            $patienttype_ids = $request->get('patienttype');
             // print_r($patienttype_id);
             $healthtool_targetaudiences_array = '';
             $healthtool_patienttype_array = '';
             
            // if target audience is not empty
            if (!empty($request->get('targetaudience'))) {
                 // get health tools according to this targetaudience_id
                
                    $healthtool_targetaudiences = DB::table('healthtool_targetaudiences')->whereIn('targetaudience_id',$targetaudience_ids)->distinct('healthtool_id')->pluck('healthtool_id');
                // }
                $healthtool_targetaudiences_array = $healthtool_targetaudiences->toArray();
               
               
            }
        
            
            // if target patient type is not empty
            if (!empty($request->get('patienttype'))) {
                 // get health tools according to this targetaudience_id
              
                    $healthtool_patienttype = DB::table('healthtool_paitienttype')->whereIn('patienttype_id',$patienttype_ids)->distinct('healthtool_id')->pluck('healthtool_id');
                    $healthtool_patienttype_array = $healthtool_patienttype->toArray();
                    // return $healthtool_patienttype_array;
               
            }
            

            // if query exists
            if (!empty($request->get('searchbar'))) {
                if (!empty($request->get('targetaudience')) && !empty($request->get('patienttype'))) {
                    $final_health_tool_ids = array_intersect($healthtool_targetaudiences_array,$healthtool_patienttype_array);
                }
                else if( !empty($request->get('targetaudience')) )
                {
                    $final_health_tool_ids = $healthtool_targetaudiences_array ;
                }
                else
                {
                     $final_health_tool_ids = $healthtool_patienttype_array ;
                }
                if ($request->get('sort') == 'asc') {
                    $healthtools = Healthtool::titleLike($request->get('searchbar'))
                                                ->whereIn('id', $final_health_tool_ids)
                                                ->userSpecific()
                                                ->active()
                                                ->ascending()
                                                ->paginate(6);

                 
                }else if($request->get('sort') == 'desc'){
                    $healthtools = Healthtool::titleLike($request->get('searchbar'))
                                                ->whereIn('id', $final_health_tool_ids)
                                                ->userSpecific()
                                                ->active()
                                                ->descending()
                                                ->paginate(6);
                   
                }

                $totalhealthtools = Healthtool::titleLike($request->get('searchbar'))
                                                ->whereIn('id', $final_health_tool_ids)
                                                ->userSpecific()
                                                ->active()
                                                ->get();

                 
            }else{
               
            // if query doesn't exists
                if (!empty($request->get('targetaudience')) && !empty($request->get('patienttype'))) {
                    $final_health_tool_ids = array_intersect($healthtool_targetaudiences_array,$healthtool_patienttype_array);
                }
                else if(!empty($request->get('patienttype')))
                {
                    $final_health_tool_ids = $healthtool_patienttype_array;
                }
                else
                {
                    $final_health_tool_ids = $healthtool_targetaudiences_array;
                }
                
                if ($request->get('sort') == 'asc') {

                    $healthtools = Healthtool::whereIn('id', $final_health_tool_ids)
                                                ->userSpecific()
                                                ->active()
                                                ->ascending()
                                                ->paginate(6);

                    
                }else if($request->get('sort') == 'desc'){
                    $healthtools = Healthtool::whereIn('id', $final_health_tool_ids)
                                                ->userSpecific()
                                                ->active()
                                                ->descending()
                                                ->paginate(6);
                }

                $totalhealthtools = Healthtool::whereIn('id', $final_health_tool_ids)
                                                ->userSpecific()
                                                ->active()
                                                ->userSpecific()
                                                ->get();

                
                // return json_encode($healthtools);
            }
           

            return view('result')->with(compact([
                'totalhealthtools',
                'healthtools',
                'targetaudiences',
                'patienttypes',
                'healthtool_page_contents',
                'healthtool_targetaudiences_array',
                'healthtool_patienttype_array',
            ]));
       }
    }

  

    public function searchhealthtool(Request $request)
    {      
            // print_r($request->get('searchbar'));
            // die; 
           // return $request->all();
            $healthtool_page_contents = Healthtoolstaticcontent::get();
            $targetaudiences = Targetaudience::where(['status' => 1])->get();
            $patienttypes = Patienttype::where(['status' => 1])->get();

       if (empty($request->get('targetaudience')) && empty($request->get('patienttype')) ) {
              $healthtools = Healthtool::titleLike($request->get('searchbar'))
                                        ->userSpecific()
                                        ->active()
                                        ->paginate(6);

              if (!empty($request->get('searchbar'))) {

                  if ($request->get('sort') == 'asc') {
                      $healthtools = Healthtool::titleLike($request->get('searchbar'))
                                        ->userSpecific()
                                        ->active()
                                        ->ascending()
                                        ->paginate(6);

                  }else if($request->get('sort') == 'desc'){
                      $healthtools = Healthtool::titleLike($request->get('searchbar'))
                                        ->userSpecific()
                                        ->active()
                                        ->descending()
                                        ->paginate(6);
                  }
                  $totalhealthtools =  Healthtool::titleLike($request->get('searchbar'))
                                        ->userSpecific()
                                        ->active()
                                        ->get();
                 
              }else{
              // no query
                  if ($request->get('sort') == 'asc') {
                      $healthtools = Healthtool::active()->userSpecific()
                                                ->ascending()
                                                ->paginate(6);
                  }else if($request->get('sort') == 'desc'){
                      $healthtools = Healthtool::active()->userSpecific()
                                                ->descending()
                                                ->paginate(6);
                  }
                  $totalhealthtools =  Healthtool::active()->userSpecific()
                                                ->get();
                                                
                
              }
              return view('result')->with(compact([
                  'totalhealthtools',
                  'healthtools',
                  'targetaudiences',
                  'patienttypes',
                  'healthtool_page_contents',
              ]));

       }else{
            $targetaudience_ids = $request->get('targetaudience');

            $patienttype_ids = $request->get('patienttype');
             // print_r($patienttype_id);

            // if target audience is not empty
            if (!empty($request->get('targetaudience'))) {
                 // get health tools according to this targetaudience_id
                $healthtool_targetaudiences_array = array();

                foreach ($targetaudience_ids as $targetaudience_id) {
                    $healthtool_targetaudiences_array[] = DB::table('healthtool_targetaudiences')->where(['targetaudience_id' => $targetaudience_id])->get();
                }
                
                $health_tool_ids = array();
                $targetaudience_count = count($healthtool_targetaudiences_array);

                for ($i=0; $i < $targetaudience_count; $i++) {
                    // check count
                    $count_two = count($healthtool_targetaudiences_array[$i]);
                    for ($j=0; $j < $count_two; $j++) { 
                        $health_tool_ids[] = $healthtool_targetaudiences_array[$i][$j]->healthtool_id;
                    }
                }
                $unique_health_tool_array = array_unique($health_tool_ids);
                $final_health_tool_ids = $unique_health_tool_array;
            }
            
            // if target patient type is not empty
            if (!empty($request->get('patienttype'))) {
                 // get health tools according to this targetaudience_id
                $healthtool_patienttype_array = array();

                foreach ($patienttype_ids as $patienttype_id) {
                    $healthtool_patienttype_array[] = DB::table('healthtool_paitienttype')->where(['patienttype_id' => $patienttype_id])->get();
                }
                $patient_type_ids = array();
                $patient_type_count = count($healthtool_patienttype_array);

                for ($i=0; $i < $patient_type_count; $i++) {
                    // check count
                    $count_two = count($healthtool_patienttype_array[$i]);
                    for ($j=0; $j < $count_two; $j++) { 
                        $patient_type_ids[] = $healthtool_patienttype_array[$i][$j]->healthtool_id;
                    }
                }
                $unique_patient_type_array = array_unique($patient_type_ids);
                $final_health_tool_ids = $unique_patient_type_array;
            }

            if (!empty($request->get('targetaudience')) && !empty($request->get('patienttype'))) {
                $final_health_tool_ids = array_unique(array_merge($unique_health_tool_array,$unique_patient_type_array));
            }
            if ($request->get('sort') == 'asc') {
                 $healthtools = Healthtool::titleLike($request->get('searchbar'))->userSpecific()
                                        ->whereIn('id', $final_health_tool_ids)
                                        ->active()
                                        ->ascending()
                                        ->paginate(6);
                
            }else if($request->get('sort') == 'desc'){
                $healthtools = Healthtool::titleLike($request->get('searchbar'))->userSpecific()
                                        ->whereIn('id', $final_health_tool_ids)
                                        ->active()
                                        ->descending()
                                        ->paginate(6);
            }
            $totalhealthtools = Healthtool::whereIn('id', $final_health_tool_ids)->userSpecific()
                                        ->active()
                                        ->descending()
                                        ->get();
            
            

            return view('result')->with(compact([
                'totalhealthtools',
                'healthtools',
                'targetaudiences',
                'patienttypes',
                'healthtool_page_contents',
                'healthtool_targetaudiences_array',
                'healthtool_patienttype_array',
            ]));
       }
    }
    
    
    public function thankyou()
    {
        if (Auth::check()) {
            if (Auth::user()->status == 0 || Auth::user()->status == NULL) {
                return view('post_register_thankyou');
            }else{
                return redirect(route('healthtools'));
            }
            
        }else{
            return redirect(route('login'));
        }
    }


    public function team_training()
    {
        return view('team_training');
    }

    public function form()
    {
        return view('form');
    }

    public function customizeTool($healthtool_id)
    {   
        $previousUrl = URL::previous();
        $healthTool = Healthtool::where(['id' => $healthtool_id])->first();
        $link = Str::slug($healthTool->title, '-');
        $client_name = Str::slug(Auth::user()->organization, '-');
        return view('customizeTool')->with(compact([
            'healthTool',
            'previousUrl',
            'link',
            'client_name'
        ]));
    }


    // public function logoUpload(Request $request)
    // {   
    //   if($_FILES["file"]["name"] != '')
    //   {
    //     $ext = $request->file->extension();
    //     $temp_name =  $request->file->getClientOriginalName();
    //     // $hashed = Hash::make($temp_name);
    //     // $hashed = base64_encode($temp_name);
    //     $uid =  date('YmdHis');
    //     $new_name = $uid;
    //     $final_name = $new_name.'.'.$ext;
    //     $save_image = $request->file->storeAs('public/userstemplogos/',$final_name);
    //     if (!$save_image) {
    //       return response()->json(['message' => 2]);
    //     }else{
    //       // image saved
    //       // find his previous image and check if it is default.png
          
    //       // get health too
    //       $getHealthTool = Healthtool::where(['id' => $request->healthtool_id])->first();
    //       $link = Str::slug($getHealthTool->title, '-');
    //       $client_name = Str::slug(Auth::user()->organization, '-');

    //       $avatar = new HealthToolsFav;
    //       $avatar->user_id = Auth::user()->id;
    //       $avatar->healthtool_slug = $request->healthtool_id;
    //       $avatar->link = $link;
    //       $avatar->avatar = 'userstemplogos/'.$final_name;
    //       $save = $avatar->save();
    //       if ($save) {
    //         return response()->json(['status' => 200,'lastId' => $avatar->id,'image' => $avatar->healthtool_slug,'url' => route('healthToolsDetail',['link' => $link,'client_name' => $client_name,'slug' => $request->healthtool_id, 'image' => $avatar->id])]);
    //       }else{
    //         return response()->json(['status' => 1, 'image' => null ]);
    //       }
    //     }
    //   }else{
    //     return response()->json(['message' => 3]);
    //   }
    // }
    
    // public function logoUploadTwo(Request $request)
    // {
    //     if ($request->orgImage == 1) {
    //         // return 1;
    //         $getHealthTool = Healthtool::where(['id' => $request->healthtool_id])->first();
            
    //         $link = Str::slug($getHealthTool->title, '-');
    //         // $link = json_decode($result)->PrettyLinkUrl;
            
    //         $client_name = Str::slug(Auth::user()->name, '-');

    //         $avatar = new HealthToolsFav;
    //         $avatar->user_id = Auth::user()->id;
    //         $avatar->healthtool_slug = $request->healthtool_id;
    //         $avatar->link = $link;
    //         $avatar->avatar = Auth::user()->avatar;
    //         $save = $avatar->save();
    //         if ($save) {
    //           return response()->json(['status' => 200,'lastId' => $avatar->id,'image' => $avatar->healthtool_slug,'url' => route('healthToolsDetail',['link' => $link,'client_name' => $client_name,'slug' => $request->healthtool_id, 'image' => $avatar->id])]);
    //         }else{
    //           return response()->json(['status' => 1, 'image' => null ]);
    //         }

    //     }
    // }
    
    public function noLogoUpload(Request $request)
    {
        $tool_id = $request->page_id;
        $file = '';
        $url = $this->getPrettyLinkUrl($file,$tool_id) ;
        echo $url;
    }




    public function logoUpload(Request $request)
    {   
        return 0;
        $fileUpload_arr = explode('base64,', $request->file);
        $hashed = $fileUpload_arr[1];
        $healthtool_id = $request->healthtool_id;
        $link = $this->getPrettyLinkUrl($hashed,$healthtool_id);
        
        
        
        return response()->json(['status' => 200,'url' => $link]);
    }

    public function logoUploadTwo(Request $request)
    {
       if ($request->orgImage == 1) {
        $getHealthTool = Healthtool::where(['id' => $request->healthtool_id])->first();
        $user_avatar = Auth::user()->avatar;
        if(file_exists(storage_path('app/public/'.$user_avatar)))
        {
            $hashed = base64_encode(file_get_contents(storage_path('app/public/'.$user_avatar)));

            $link = $this->getPrettyLinkUrl($hashed,$request->healthtool_id);

            return response()->json(['status' => 200,'url' => $link]);
        }
        else
        {
            return 1;
        }

        }
    }

    public function healthToolsDetail(Request $request, $link, $client_name, $healthtool_id)
    {   
        // return $request->all();
       
        
        $healthTool = Healthtool::where(['id' => $healthtool_id])->first();
        if ($request->orgImage) {
             if(Auth::check()){
             $imageInfo = User::where(['id' => Auth::user()->id])->first();
            }else{
                 $imageInfo = null;
            }
           
        }else{
            if ($request->image) {
                $imageInfo = HealthToolsFav::where(['id' => $request->image])->first();
            }else{
                $imageInfo = null;
            }    
        }
        
        // return $healthTool;
        return view('customizeHealthTool')->with(compact([
            'healthTool',
            'imageInfo'
        ]));
    }

    // public function postToFav(Request $request)
    // {   
    //     // return $request->all();
    //     // return Auth::user()->id;
    //     if ($request->lastId) {
    //         $favTool = HealthToolsFav::where(['id' => $request->lastId])->first();
    //         $favTool->is_fav = 1;
    //         $save = $favTool->save();
    //     }else{
    //         if ($request->org_image) {
    //             $newfav = new HealthToolsFav; 
    //             $newfav->user_id = Auth::user()->id;
    //             $newfav->healthtool_slug = $request->id;
    //             $newfav->org_image = $request->org_image;
    //             $newfav->is_fav = 1;
    //             $save = $newfav->save();
                
    //         }else{
    //             $favTool = HealthToolsFav::where(['user_id' => Auth::user()->id,'healthtool_slug' =>$request->id])->first();
    //             if (empty($favTool)) {
    //                 $newfav = new HealthToolsFav; 
    //                 $newfav->user_id = Auth::user()->id;
    //                 $newfav->healthtool_slug = $request->id;
    //                 $newfav->is_fav = 1;
    //                 $save = $newfav->save();
    //             }else{
    //                 $favTool->is_fav = 1;
    //                 $save = $favTool->save();
    //             }    
    //         }
    //     }
        
    //     if ($save) {
    //       return response()->json(['status' => 200 ]);
    //     }else{
    //       return response()->json(['status' => 500 ]);
    //     }
    // }
    public function postToFav(Request $request)
    {
        $data = $request->all();
        $data['is_fav'] = 1 ;
        $save = HealthToolsFav::create($data);

        if ($save) {
            return response()->json(['status' => 200 ]);
        }else{
            return response()->json(['status' => 500 ]);
        }
    }

    public function deleteFromFav(Request $request)
    {   
        // return Auth::user()->id;
        $favTool = HealthToolsFav::where(['id' => $request->id])->first();
        $delete = $favTool->delete();
        
        if ($delete) {
          return response()->json(['status' => 200 ]);
        }else{
          return response()->json(['status' => 500 ]);
        }
    }

    public function qualityCare()
    {   
        $qualityCare = QualityCare::first();
        return view('qualityCare')->with(compact(['qualityCare']));
    }
    
    public function underReview()
    {   
        return view('underReview');
    }
    
    public function getTools(Request $request)
    {
        if($request->token == 'liver@land#123!')
        {
            $tools = DB::table('user_health_tools')->get();
            return response()->json(['data' => $tools]);  
        }
        else
        {
            return response()->json(['status' => 401 , 'message' => 'Unauthenticated']);
        }
    }

    public function getUser(Request $request)
    {
        if($request->token == 'liver@land#123!')
        {
            $users = User::get();
            return UserHealthToolCollection::collection($users);
        }
        else
        {
            return response()->json(['status' => 401 , 'message' => 'Unauthenticated']);
        }
    }
    
    public function wrestlingthemonster()
    {   
        $content = WrestlingMonster::first();
        return view('wrestlingthemonster')->with(compact('content'));
    }
    
    public function annualTrendsReport()
    {   
        return view('annualTrendsReport');
    }
} 
