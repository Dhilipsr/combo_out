<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request; 
use DB;
use Auth;
use App\DepartmentsHealthtool;

class AjaxSearchController extends Controller
{
    // function action(Request $request)
    // {
    //  if($request->ajax())
    //  {
    //   $output = '';
    //   // $output_two = '';
    //   // $output_three = '';
    //   $query = $request->get('query');
    //   if($query != '')
    //   {

    //   $data = DB::table('healthtools')
    //      ->where('title', 'like', '%'.$query.'%')
    //      ->where(['status' => 1])
    //       ->wherein('id',DepartmentsHealthtool::where('department_id',Auth::user()->department_id)->pluck('healthtool_id'))
    //      ->orderBy('title', 'asc')
    //      ->get();
    //   }
    //   else
    //   {
    //   $data = DB::table('healthtools')
    //     ->wherein('id',DepartmentsHealthtool::where('department_id',Auth::user()->department_id)->pluck('healthtool_id'))
    //      ->orderBy('title', 'asc')
    //      ->where(['status' => 1])->get();
    //   }
    //   // $total_row = $data->count() + $ebwdata->count() + $guidelines->count();
    //   $total_row = $data->count();
    //   if($total_row > 0)
    //   {
    //   foreach($data as $row)
    //     {
    //     if ((Auth::check())) {
    //     //   $row->pretty_link = route('login');
    //       $redirection = route('customizeHealthTools',['healthtool_id' => $row->id]);
    //     }else{
    //         $redirection = route('login');
    //     }
    //     $output .= '
    //     <li><a href='.$redirection.'>'.$row->title.'</a></li>';
    //   }

    //   // foreach($ebwdata as $row_two)
    //   // {
    //   //  if (!(Auth::check())) {
    //   //    $row_two->pretty_link = route('login');
    //   //  }
    //   //  $output_two .= '
    //   //  <li><a href='.$row_two->pretty_link.'>'.$row_two->title.'</a></li>';
    //   // }

    //   // foreach($guidelines as $row_three)
    //   // {
    //   //  if (!(Auth::check())) {
    //   //    $row_three->pretty_link = route('login');
    //   //  }
    //   //  $output_three .= '
    //   //  <li><a href='.$row_three->preety_link.'>'.$row_three->title.'</a></li>';
    //   // }
    //   }
    //   else
    //   {
    //   $output = '
    //   <tr>
    //     <td align="center" colspan="5">No Data Found</td>
    //   </tr>
    //   ';

    //   // $output_two = '
    //   // <tr>
    //   //  <td align="center" colspan="5">No Data Found</td>
    //   // </tr>
    //   // ';
    //   //  $output_three = '
    //   // <tr>
    //   //  <td align="center" colspan="5">No Data Found</td>
    //   // </tr>
    //   // ';
    //   }
    //   $data = array(
    //   'table_data'  => $output,
    //   'total_data'  => $total_row,
    //   // 'table_data_two'  => $output_two,
    //   // 'table_data_three'  => $output_three,

    //   );

    //   echo json_encode($data);


    //  }
    // }
    
    function action(Request $request)
    {
     if($request->ajax())
     {
      $output = '';
     
      $query = $request->get('query');
      if($query != '')
      {

       $data = DB::table('healthtools')
         ->where('title', 'like', '%'.$query.'%')
         ->where(['status' => 1])
         ->orderBy('title', 'asc')
         ->wherein('id',DepartmentsHealthtool::where('department_id',Auth::user()->department_id)->pluck('healthtool_id'))
         ->get();
      }
      else
      {
       $data = DB::table('healthtools')
         ->orderBy('title', 'asc')
         ->wherein('id',DepartmentsHealthtool::where('department_id',Auth::user()->department_id)->pluck('healthtool_id'))
         ->where(['status' => 1])->get();
      }
 
      $total_row = $data->count();
      if($total_row > 0)
      {
       foreach($data as $row)
        {
        if ((Auth::check())) {
   
          $redirection = route('customizeHealthTools',['healthtool_id' => $row->id]);
        }else{
            $redirection = route('login');
        }
        $output .= '
        <li><a href='.$redirection.'>'.$row->title.'</a></li>';
       }
      }
      else
      {
       $output = '
                  <tr>
                    <td align="center" colspan="5">No Data Found</td>
                  </tr>
                  ';

      }
      $data = array(
       'table_data'  => $output,
       'total_data'  => $total_row,
      
      );

      echo json_encode($data);


     }
    }
}
