<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function serviceData($slug)
    {
    	$content = Service::with('sections')
		    		->whereSlug($slug)
		    		->first();

		return $content ;
    }

    public function servicePage($slug)
    {
    	switch ($slug) {
    		case 'screen-patients':
    			return 'screen_patients';
    			break;

    		case 'define-an-episode':
    			return 'define_episode';
    			break;

    		case 'coordinat_care':
    			return 'coordinate_care';
    			break;

    		default:
    			return 'screen_patients';
    			break;
    	}
    }
}
