<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class CheckUserStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        // check if logged in
        if (Auth::check()) {
            
            if (Auth::user()->status == 1) {
               return $next($request);
            }else{
               Auth::logout(); 
               return redirect()->route('underReview');
            }
        }else{
            Auth::logout(); 
            return redirect()->route('login')->with(['message' => "Please log in.!", 'alert-type' => 'error']);
        }   
    }
}
