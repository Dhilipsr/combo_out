<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Role;
use App\Organizationtype;

class UserHealthToolCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $role = Role::where('id',$this->role_id)->first();
        $organization = Organizationtype::where('id',$this->organization_id)->first();
        return 
        [
            'role_id' => $role != '' ? $role->display_name : '',
            'name' => $this->name,
            'email' => $this->email,
            'avatar' => asset('storage/.'.$this->avatar),
            'lname' => $this->lname,
            'organization_type' => $organization != '' ? $organization->type : '' ,
            'organization' => $this->organization,
            'job_title' => $this->job_title,
            'contact' => $this->contact,
            'address' => $this->address,
            'salix_manager_name' => $this->salix_manager_name,
            'status' => $this->status == 1 ? 'Active' : 'Pending',
            'last_login' => $this->last_login,
            'last_login_timezone' => $this->last_login_timezone,
            'timezone' => $this->timezone,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_at_timezone' => $this->created_at_time_zone,
            'updated_at_timezone' => $this->updated_at_timezone,
        ];
    }
}
