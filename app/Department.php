<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Department extends Model
{
     public function healthtools()
     {
         return $this->belongsToMany('App\Healthtool');
     }
}
