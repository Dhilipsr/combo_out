<?php

namespace App;

use App\HomePageContent;
use Illuminate\Database\Eloquent\Model;


class Homebannercontent extends Model
{
    protected $table = 'homebannercontents';

    public function page()
    {
    	$this->belongsTo(HomePageContent::class)->onDelete()->cascade();
    }
}
