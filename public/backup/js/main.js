$(document).ready(function() {

    stickyHeader();
    $(window).scroll(function() {
        stickyHeader();
    });

    /*  $('.select2').select2({
          placeholder: "Select an option",
      });
      $(".sortSel").select2({
          placeholder: "Sort By"
      });*/

    var dataIdRef;
    $(document).on('click', '.htFiltHdSpan', function(e) {
        e.stopPropagation();
        let dataId = $(this).data('id');
        // console.log(dataId);
        if (dataIdRef == undefined || dataIdRef != dataId) {
            $('.htFiltHdSpan').css('opacity', 0.5);
            $(this).css('opacity', 1);
            $('.filtIntBox').show();
            $('.htFiltBody').hide();
            $('.htFiltSearchBox').hide();
            $('.htFiltSortBox').hide();
            $('.htFiltBody' + dataId).show();
            dataIdRef = dataId;
        } else {
            filterClose();
        }
    })
    $(document).on('click', 'body', function() {
        filterClose();
    })

    function filterClose() {
        $('.htFiltHdSpan').css('opacity', 1);
        $('.filtIntBox').hide();
        $('.htFiltBody').show();
        $('.htFiltSearchBox').show();
        $('.htFiltSortBox').show();
        dataIdRef = undefined;
    }

    $(document).on('click', '.filtIntBox', function(e) {
        e.stopPropagation();
    })

    $(document).on('click', '.srchBtn', function(e) {
        $('.searchContainer').fadeIn(300);
        $('body').css('overflow', 'hidden');

    })
    $(document).on('click', '.srchClose, .searchContainer', function(e) {
        $('.searchContainer').fadeOut(300);
        $('body').css('overflow', 'auto');
    })
    $(document).on('click', '.srch', function(e) {
        e.stopPropagation();
    })

    /* Menu */
    $(document).on('click', '.menu-icon', function() {
        $('.mob-menu-up').fadeIn();
    })
    $(document).on('click', '.mob-close', function() {
        $('.mob-menu-up').fadeOut();
    })
    /* //Menu */




    var button = $('.refrences_sec h5');
    var slider = $('.slider');

    button.on('click', function(e) {

        if (slider.hasClass('open')) {
            button.toggleClass('open');
            slider.toggleClass('open');
        } else {
            slider.toggleClass('open');
            button.toggleClass('open');
        }

    });

    /* SWIPER SLIDER */
    var swiper = new Swiper('.swiper-container', {
        spaceBetween: 30,
        effect: 'fade',

         autoplay:false,
        fadeEffect: {
            crossFade: true
        },
        autoplay: {
            delay: 3000,
        },
        speed: 3000,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },

    });



});

function stickyHeader() {
    var sticky = $('header'),
        scroll = $(window).scrollTop();

    if (scroll >= 40) sticky.addClass('fixHeader');
    else sticky.removeClass('fixHeader');
}