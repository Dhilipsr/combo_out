@extends('layouts.layout')

@section('title')
Home
@endsection

@section('content')
<!-- Start Here -->
<div class="banner-container">
    <div class="banner-container__top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Health Tools</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section">
    <div class="container">
        <div class="row  justify-content-center">
            <div class="col-lg-10 col-12">
                <h3>Find the appropriate health tools for your patients and their caregivers quickly and
                    easily by using the filters below.</h3>
                <h3>If you want to put your logo on these tools, contact your Salix Account Manager.</h3>
            </div>
        </div>
    </div>
</div>

<div class="htFilter">
    <div class="container">
        <div class="htFilterRow">
            <div class="htFiltHead htFiltHead1">
                <span class="htFiltHdSpan" data-id="1">Target Audience <img src="img/arrow.png"
                        alt="img" /></span>
            </div>
            <div class="htFiltHead htFiltHead2">
                <span class="htFiltHdSpan" data-id="2">Patient Type <img src="img/arrow.png"
                        alt="img" /></span>
            </div>
            <div class="htFiltHead htFiltSearchBox">
                <input type="text" placeholder="Search" class="htFiltSearch" />
            </div>
            <div class="htFiltHead htFiltSortBox">
                <select class="select2 form-field sortSel" placeholder="Sort By">
                    <option></option>
                    <option value="1">A - Z</option>
                    <option value="2">Z - A</option>
                </select>
            </div>
        </div>
        <div class="filtIntBox">
            <div class="filtInt">
                <ul class="htFiltBody htFiltBody1">
                    <li>
                        <div class="check-box">
                            <input type="checkbox" id="chk11">
                            <label for="chk11">Patient</label>
                        </div>
                    </li>
                    <li>
                        <div class="check-box">
                            <input type="checkbox" id="chk12">
                            <label for="chk12">Caregiver</label>
                        </div>
                    </li>
                    <li>
                        <div class="check-box">
                            <input type="checkbox" id="chk13">
                            <label for="chk13">Provider</label>
                        </div>
                    </li>
                </ul>
                <ul class="htFiltBody htFiltBody2">
                    <li>
                        <div class="check-box">
                            <input type="checkbox" id="chk21">
                            <label for="chk21">OHE Newly diagnosed</label>
                        </div>
                    </li>
                    <li>
                        <div class="check-box">
                            <input type="checkbox" id="chk22">
                            <label for="chk22">OHE Patient on treatment</label>
                        </div>
                    </li>
                    <li>
                        <div class="check-box">
                            <input type="checkbox" id="chk23">
                            <label for="chk23">OHE Patient at discharge</label>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="filtBtns">
                <button class="filtBtn">Filter</button>
                <a href="#">Clear Filter</a>
            </div>
        </div>
        <p class="filtResult">Your Results 10</p>
    </div>
</div>

<div class="htContainer">
    <div class="htContainerIn">
        <div class="htBox">
            <div class="htBoxIn">
                <div class="htImg"><img src="img/ht1.jpg" alt="img" /></div>
                <div class="htCont">
                    <div class="htCont__in">
                        <h2>Important Signs and Symptoms for Patients With Chronic Liver Disease</h2>
                        <p>Mental and physical symptoms of HE that patients with chronic liver disease
                            should be aware of.</p>
                    </div>
                    <button class="button">View/Print</button>
                </div>
            </div>
        </div>
        <div class="htBox">
            <div class="htBoxIn">
                <div class="htImg"><img src="img/ht2.jpg" alt="img" /></div>
                <div class="htCont">
                    <div class="htCont__in">
                        <h2>Discharge Instructions for Patients With Hepatic Encephalopathy</h2>
                        <p>Patient tips for managing hepatic encephalopathy once they are discharged
                            from
                            the hospital.</p>
                    </div>
                    <button class="button">View/Print</button>
                </div>
            </div>
        </div>
        <div class="htBox">
            <div class="htBoxIn">
                <div class="htImg"><img src="img/ht1.jpg" alt="img" /></div>
                <div class="htCont">
                    <div class="htCont__in">
                        <h2>Important Signs and Symptoms for Patients With Chronic Liver Disease</h2>
                        <p>Mental and physical symptoms of HE that patients with chronic liver disease
                            should be aware of.</p>
                    </div>
                    <button class="button">View/Print</button>
                </div>
            </div>
        </div>
        <div class="htBox">
            <div class="htBoxIn">
                <div class="htImg"><img src="img/ht2.jpg" alt="img" /></div>
                <div class="htCont">
                    <div class="htCont__in">
                        <h2>Discharge Instructions for Patients With Hepatic Encephalopathy</h2>
                        <p>Patient tips for managing hepatic encephalopathy once they are discharged
                            from
                            the hospital.</p>
                    </div>
                    <button class="button">View/Print</button>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <ul class="pagin">
        <li class="paginNP">Prev</li>
        <li>1</li>
        <li class="active">2</li>
        <li>3</li>
        <li class="paginNP">Next</li>
    </ul>
</div>
@endsection

@section('page-level-js')
@endsection
