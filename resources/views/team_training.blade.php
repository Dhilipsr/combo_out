@section('title')

Field Tower Team Training

@endsection

@extends('layouts.layout')

@section('content')

<!-- Start Here -->
<div class="banner-container">
    <div class="banner-container__top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Field & Tower Team Training Modules</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section eved-start">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-10 col-12">
                <h3>These <span>Training Modules</span> are designed to guide you through interactions with
                    your
                    customers, from starting a population
                    health conversation through enrolling them as a member of the ACE platform.</h3>
            </div>
            <div class="col-12">
                <div class="guideline-links">
                    <ul>
                        <li>
                            <a href="{{asset('pdf/overview-of-ace-ehr-platform.pdf')}}" target="_blank"><img src="{{asset('img/pdf.png')}}" alt="img" /> Overview of ACE EHR
                                Platform</a>
                        </li>
                        <li>
                            <a href="{{asset('pdf/introduction-to-ace-overview-deck.pdf')}}" target="_blank"><img src="{{asset('img/pdf.png')}}" alt="img" /> Introduction to ACE Overview
                                Deck
                            </a>
                        </li>
                        <li>
                            <a href="{{asset('pdf/introduction-to-care-pathway-1.pdf')}}" target="_blank"><img src="{{asset('img/pdf.png')}}" alt="img" />Introduction to Care
                                Pathway 1</a>
                        </li>
                        <li>
                            <a href="{{asset('pdf/introduction-to-care-pathway-2.pdf')}}" target="_blank"><img src="{{asset('img/pdf.png')}}" alt="img" />Introduction to Care
                                Pathway 2</a>
                        </li>
                        <li>
                            <a href="{{asset('pdf/introduction-to-care-pathway-3-and-transitions-of-care-presentation.pdf')}}" target="_blank"><img src="{{asset('img/pdf.png')}}" alt="img" />Introduction to Care
                                Pathway 3 and Transitions of Care Presentation</a>
                        </li>
                        <li>
                            <a href="{{asset('pdf/getting-ready-to-put-it-all-together.pdf')}}" target="_blank"><img src="{{asset('img/pdf.png')}}" alt="img" />Getting Ready to Put
                                It All Together</a>
                        </li>
                        <li>
                            <a href="{{asset('pdf/hands-on-application-of-segmenting.pdf')}}" target="_blank"><img src="{{asset('img/pdf.png')}}" alt="img" />Hands-on Application
                                of Segmenting</a>
                        </li>
                        <li>
                            <a href="{{asset('pdf/identifying-who-to-go-to.pdf')}}" target="_blank"><img src="{{asset('img/pdf.png')}}" alt="img" />Identifying “Who to Go
                                To”</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection



@section('extra-js')

@endsection