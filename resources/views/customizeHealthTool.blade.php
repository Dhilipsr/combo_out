
@section('title')
{{$healthTool->title}}
@endsection
@extends('layouts.custom_htmls.layout')
@section('content')
<!-- Header Starts -->
<header>
    @php
        $url = Request::url();
        $checkImage = Request::get('image');
        if($checkImage){
            $url = $url.'/?image='.$checkImage;
        }
        
        // check if url database exists
        $checkUrl = DB::table('user_health_tools')->where(['url' => $url])->first();
        if(empty($checkUrl)){
            if($imageInfo && $imageInfo->org_image == 1){
                $custom_logo = null;
                if(Auth::user()->avatar){
                    $company_logo = asset('storage/'.Auth::user()->avatar);
                }else{
                    $company_logo = null;
                }
                
            }else{
                if(!empty($imageInfo)){
                    $custom_logo = asset('storage/'.$imageInfo->avatar);    
                }else{
                    $custom_logo = null;
                }
                
                $company_logo = null;
            }
            $title = $healthTool->title;

            $slug = str_slug($title, '-');
            $check = DB::table('user_health_tools')->insert(
                ['name' => $title, 'slug' => $slug,'url' => $url,'custom_logo' => $custom_logo]
            );
          
        }
    @endphp
    <div class="container">
        @if($imageInfo && $imageInfo->org_image == 1)
                @if(Auth::user()->avatar)
		        <div class="member-logo"><img src="{{asset('storage/'.Auth::user()->avatar)}}" alt="Member Logo" /></div>
		        @endif
    	@else
        	@if(!empty($imageInfo->avatar))
    	        <div class="member-logo"><img src="{{asset('storage/'.$imageInfo->avatar)}}" alt="Member Logo" /></div>
    	    @else
    	    @endif
    	@endif
    </div>
</header>
<!-- Header Ends -->
{!! $healthTool->html !!}

@endsection
