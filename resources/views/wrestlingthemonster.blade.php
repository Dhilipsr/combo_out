@extends('layouts.layout')

@section('title')
Wrestling the Monster: Living With Hepatic Encephalopathy
@endsection

@section('content')

    <!-- Main Section -->
    <div class="main_section">
        <div class="container">
            <div class="main-title margin-b-30 margin-t-30">
                <h2>{{$content->title}}</h2>
            </div>
            <div class="text-container">
                <p>
                 {{$content->description}}
                </p>
            </div>
            
            <div class="ytcontainer">
                <iframe 
                class="responsive-iframe"
                width="100%" 
                
                src="{{$content->link}}" 
                title="YouTube video player" 
                frameborder="0" 
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" 
                allowfullscreen></iframe>
            </div>

        </div>
    </div>
    

  @endsection

@section('page-level-js')
@endsection