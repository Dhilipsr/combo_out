@section('title')

Evidence-based Workflow

@endsection

@extends('layouts.layout')

@section('content')

<!-- Start Here -->

<div class="banner-container">

    <div class="banner-container__top">

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <h2>Evidence-based Workflow</h2>

                </div>

            </div>

        </div>

    </div>

</div>

<div class="section eved-start">

    <div class="container">

        <div class="row justify-content-center">

            <div class="col-lg-10 col-12">

                @foreach($ebw_page_contents as $ebw_page_content)

                    <h3>{!! $ebw_page_content->content !!}</h3>

                @endforeach

            </div>

            <div class="col-12">

                <div class="evidence-links">

                    <ul>

                        @foreach( $workflows as $workflow )

                            <li>

                                <h2>{{$workflow->name}}</h2>

                                <a href="#" data-toggle="modal" target="_blank" class="click_check" data-target="#guideModal_{{$workflow->id}}" data-url={{$workflow->preety_link}}>{{$workflow->title}}</a>

                            </li>

                        @endforeach

                    </ul>

                </div>

            </div>

        </div>

    </div>

</div>

<div class="ehr-contact">

    <div class="container">

        <div class="row">

            <div class="col-12">

                <h3> See the <span>patient engagement plugins</span> that go with each Care Pathway.</h3>

                <a href="{{route('ehrplugin')}}" class="button">See Now</a>

            </div>

        </div>

    </div>

</div>

{{-- Modal --}}
@foreach( $workflows as $workflow )
    <div id="guideModal_{{$workflow->id}}" class="modal fade" role="dialog">
        <div class="modal-dialog modal-box">
            <div class="model-close" data-dismiss="modal"><img src="{{asset('img/close-white.png')}}" alt="img" /></div>
            <div class="row guidePop">
                <div class="col-md-12">
                    <h3>Thank you for visiting the ACE website;<br> you are now leaving our site</h3>
                    <ul>
                        <li><a href="{{route('evidenceflow')}}"><img src="{{asset('img/arrow-white-left.png')}}" alt="img" /> &nbsp;&nbsp; Go Back</a>
                        </li>
                        <li><a href="{{$workflow->pretty_link}}" target="_blank">Continue &nbsp;&nbsp; <img src="{{asset('img/arrow-white-right.png')}}" alt="img" /></a>
                        </li>
                    </ul>
                    <p class="text-center text-white">The website you are about to visit is not affiliated with Salix Pharmaceutical or its affiliated entities, and is not responsible for the content, format, maintenance,or policies of the website you are about to visit. Salix Pharmaceutical or its affiliated entities do not monitor non-affiliated websites for accuracy. This link does not imply endorsement or support of any program, products, or services associated with the website.</p>
                </div>
                <div class="col-md-12 text-center">
                    <div class="check-box">
                        <input type="checkbox" class="do-not-show"  id="chk_{{$workflow->id}}">
                        <label for="chk_{{$workflow->id}}">Don’t show me this message again.</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach
@endsection

@section('extra-js')
<script type="text/javascript">
  $(document).ready(function(){
    $('.click_check').click(function(e){
        var local_check = localStorage.getItem("donotshowevidence");
          if (local_check == 1) {
            var link = $(this).attr('data-url');
            window.location.href = link;
             e.stopPropagation();
          }else{

          }
    });
    $(".do-not-show").click(function(){
    
      if($(this). prop("checked") == true) {
        localStorage.setItem("donotshowevidence", "1");
      }else{
        localStorage.removeItem("donotshowevidence"); 
        
      }
    });
  });
</script>
@endsection