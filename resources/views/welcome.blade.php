@section('title')
Home
@endsection
@extends('layouts.layout')
@section('content')
<!-- Start Here -->
<!-- Banner Section -->

    <section class="slider_sec">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                @php
                    $sr = 1;
                @endphp
                @foreach( $banner_contents as $banner )
                <div class="swiper-slide">
                    <img src="{{ asset('storage/'.$banner->img) }}" alt="{{ $banner->alt_tag }}" class="ele img-0{{$sr}}">
                    <article class="slider-points">
                        {!! $banner->content !!}
                        @if($loop->last)
                            <a href="{{ route('healthtools') }}">Get Resources</a>
                        @endif
                    </article>
                </div>
                @php
                    $sr++
                @endphp
                @endforeach
                {{-- <div class="swiper-slide">
                    <img src="{{asset('img/slider-02.png')}}" class="ele img-02">
                    <article class="slider-points">
                        <h5> <span class="big-2">Chronic Liver Disease</span> <br>can lead to
                            <span class="big-5 yel_colr cirr"><b class="super ">Cirrhosis<sup>2</sup></b></span>
                        </h5>
                    </article>
                </div>
                <div class="swiper-slide ">
                    <img src="{{asset('img/slider-03.png')}}" class="ele img-03">
                    <article class="slider-points point-3">
                        <h5>
                            <span class="big-3">80%</span> of patients with <span class="big-4">Cirrhosis</span> will
                            develop<br>
                            <span class="big-5 yel_colr hep">Hepatic Encephalopathy <b class="super">(HE)<sup>3</sup></b></span>
                        </h5>
                    </article>
                </div> 
                <div class="swiper-slide">
                    <img src="{{asset('img/slider-04.png')}}" class="ele img-04">
                    <article class="slider-points left-txt">
                        <h5> <span class="big-3">53%</span> of hospitalized patients with <span
                                class="big-1 yel_colr">overt</span> <span class="big-2 yel_colr">HE</span> are<br>
                            <span class="big-1 yel_colr">readmitted</span> within 30 <b class="super">days<sup>4</sup></b>
                        </h5>

                        <h5>
                            <span class="big-3">40%</span> of those <span class="big-2 yel_colr">readmissions</span> can
                            be
                            <span class="big-3 yel_colr pre"><b class="super"> prevented<sup>5</sup></b></span>
                        </h5>
                    </article>

                </div>

                <div class="swiper-slide">
                    <img src="{{asset('img/slider-05.png')}}" class="ele img-05">
                    <article class="slider-points">
                        <h5>
                            Patients who have been hospitalized with <span class="big-1 yel_colr">overt</span> <span
                                class="big-2 yel_colr">HE</span> have
                            a
                            <span class="big-4 ">42%</span> probability of <span class="big-4 yel_colr">survival</span>
                            at 1
                            <b class="super">year<sup>6</sup></b>
                        </h5>
                    </article>
                </div> 
                <div class="swiper-slide">
                    <img src="{{asset('img/slider-06.png')}}" class="ele img-06">
                    <article class="slider-points">
                        <h5>Here’s how <br> you can <span class="big-3">Impact Care</span> for patients with<br>
                            <span class="big-5 yel_colr">
                                Chronic Liver Disease</span>
                        </h5>
                        <a href="">Get Resources</a>
                    </article>

                </div> --}}
            </div>
            <!-- <div class="swiper-pagination swiper-pagination-white"></div> -->
        </div>
    </section>

    <!-- Banner Section Over-->
    <!-- References Section -->
    <section class="refrences_container">
        <div class="refrences_sec">
            <h5>References </h5>
        </div>
        <div class="slider ">
            <div class="container">
                {!! $page_contents->ref !!}
                {{-- <ol>
                    <li>Chronic Liver Disease Foundation. About CLDF mission statement.
                        <a href="https://chronicliverdisease.org/about/mission.cfm."
                            target="_blank">https://chronicliverdisease.org/about/mission.cfm.</a>
                        Accessed October 2, 2019.</li>
                    <li>Vilstrup H, Amodio P, Bajaj J, et al. Hepatic encephalopathy in chronic liver disease: 2014
                        Practice
                        Guideline by the
                        American Association for the Study of Liver Diseases and the European Association for the Study
                        of
                        the Liver.
                        Hepatology. 2014;60(2):715-735.</li>
                    <li>Cichoz-Lach H, Michalak A. Current pathogenetic aspects of hepatic encephalopathy and
                        noncirrhotic
                        hyperammonemic
                        encephalopathy. World J Gastroenterol. 2013;19(1):26-34.</li>
                    <li>Data on file (Forte).</li>
                    <li>Tapper EB, Halbert B, Mellinger J, et al. Rates of and reasons for hospital readmissions in
                        patients
                        with cirrhosis:
                        a multistate population-based cohort study. Clin Gastroenterol Hepatol. 2016;14(8):1181-1188.
                    </li>
                    <li>Wolf D. Hepatic encephalopathy. Medscape. <a
                            href="https://chronicliverdisease.org/about/mission.cfm."
                            target="_blank">https://emedicine.medscape.com/article/186101-overview.</a>
                        Accessed October 2,
                        2019.</li>
                </ol> --}}
            </div>
        </div>
    </section>
    <!-- References Section Over-->

    <!-- Main Section -->
    <div class="main_section">
        <div class="container">
            <h2 class="blue_tag">{!! $page_contents->subtitle !!}</h2>

            {{-- <h3 class="sub-tag"> --}}
                {!! $page_contents->description !!}
            {{-- </h3> --}}
        </div>
    </div>
    <!-- Main Section Over-->
    
    <div class=" section  pt-0">
        <!-- container class added -->
        <div class="container">


            <div class="htBoxIn m-auto">
                <div class="htImg"><img src="img/mag.jpg" alt="img"></div>
                <div class="htCont">
                    <div class="htCont__in">

                        <p>Discover the latest trends
                            in chronic liver disease
                            care in the first edition
                            of the <a href="{{route('annualTrendsReport')}}" target="_blank">Liver Health
                                Annual Trends Report.</a> </p>
                    </div>

                </div>
            </div>




        </div>
    </div>
    
<!-- Ends Here -->
@endsection

@section('page-level-js')
@endsection