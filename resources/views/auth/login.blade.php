@extends('layouts.layout')
@section('title')
Log In
@endsection
@section('content')
{{-- Start Here  --}}
{{-- <div class="banner-container">
    <div class="banner-container__top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Log In</h2>
                </div>
            </div>
        </div>
    </div>
</div> --}}

<div class="login-box section">
    <div class="default-box">
        <h2>Log in to LIVER<span>HEALTH</span>NOW</h2>
        <p>Welcome back! Enter your email address and password below<br> for access to your account.</p>
    </div>
    <div class="container pad">
        <div class="row">
            <div class="col-md-12">
                 <p class="ind-txt float-right"><span>*</span> Required field</p>
            </div>
        </div>
        <form method="POST" action="{{route('login')}}">
            <div class="row">
                @csrf
                <div class="col-md-6 form-box @error('email') error-box @enderror">
                    <label class="req">Enter Email Address</label>
                    <input type="email" class="form-field" name="email" id="email" value="{{ old('email') }}" required="required">
                    @error('email')
                    <div class="error"><img src="{{asset('img/error-arrow.png')}}" alt="error" />
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="col-md-6 form-box @error('password') error-box @enderror">
                    <label class="req">Password</label>
                    <input type="password" class="form-field" name="password" id="password" required="required" >
                    @error('password')
                    <div class="error"><img src="{{asset('img/error-arrow.png')}}" alt="error" />
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="col-md-12">
                    <div class="check-box">
                        <input type="checkbox" id="chk1" name="remember">
                        <label for="chk1">Keep me signed in</label>
                    </div>
                </div>
                <div class="col-md-12 form-box">
                    <button id="login">Sign In</button>
                </div>
                <div class="col-md-12 forgot">
                    <a data-toggle="modal" data-target="#myModal">Forgot your password?</a>
                </div>
            </div>
        </form>
    </div>
    <div class="default-box login-register">
        <div class="form-box">
            <h2>Don't have an account?</h2>
            <p>Haven’t joined yet? Sign up now to get OHE health tools, evidence-based workflows, <br>
                guidelines, and links.</p>
        </div>
        <div class="form-box">
            <a href="{{route('register')}}" class="button">Register</a>
        </div>
    </div>
</div>
<!-- Modal Starts here-->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-box">
        <div class="model-close" data-dismiss="modal"><img src="{{asset('img/close-white.png')}}" alt="img" /></div>
        <div class="default-box">
            <h2>Password Recovery</h2>
            <p>Please enter your email address. <br> You will receive a link via email to reset your password. </p>
        </div>
        <form method="POST" action="{{ route('password.email') }}">
            <div class="row justify-content-center mt-3">
                <div class="col-md-6 form-box error-box">
                    <p class="ind-txt float-right"><span>*</span> Required field</p>
                    <!-- <label class="req">Enter Email Address</label> -->
                    @csrf
                    <input type="email" class="form-field" name="email" required placeholder="Enter Email Address">
                    <div class="error"><img src="{{asset('img/error-arrow.png')}}" alt="error" /> This field is required
                    </div>
                    <div class="form-box mt-4">
                        <button class="button">Request New Password</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- Modal Ends Here -->

@endsection
@section('extra-js')
<script type="text/javascript">
    $(document).ready(function() {
        $('#login').click(function(e) {
            var email = $('#email').val();
            var password = $('#password').val();
            var email = $('#email').val();
            if (email == "") {
                $('#email').parents('.form-box').addClass('error-box');
                e.preventDefault();
            }

            if (password == "") {
                $('#password').parents('.form-box').addClass('error-box');
                e.preventDefault();
            }
        });
    });
</script>
@endsection
