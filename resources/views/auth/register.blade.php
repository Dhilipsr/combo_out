@extends('layouts.layout')

@section('title')
Register
@endsection

@section('content')
<!-- Start Here -->
<div class="banner-container">
    <div class="banner-container__top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>LiverHealth Registration </h2>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="reg-form section">
    <div class="container">
        <div class="row">
            <h3>Fill in the information below to request access to the resources on this website. This site
                is intended for US health care professionals only.</h3>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p class="ind-txt float-right"><span>*</span> Required Field</p>
            </div>
        </div>
        <form method="POST" action="{{route('register')}}">
            <div class="row">
                @csrf
                <div class="col-md-6 form-box @error('password') error-box @enderror">
                    <label class="req">Password</label>
                    <input type="password" class="form-field"  name="password">
                    <div class="error"><img src="{{asset('img/error-arrow.png')}}" alt="error" />
                        @error('password')
                            {{ $message }}
                        @enderror
                    </div>
                </div>
                <div class="col-md-6 form-box">
                    <label class="req">Confirm Password</label>
                    <input type="password" class="form-field" name="password_confirmation">
                    <div class="error"><img src="{{asset('img/error-arrow.png')}}" alt="error" /> This field is required
                    </div>
                </div>
                <div class="col-md-6 form-box @error('email') error-box @enderror">
                    <label class="req">Email Address</label>
                    <input type="email" class="form-field"  name="email" value="{{ old('email') }}">
                    <div class="error"><img src="{{asset('img/error-arrow.png')}}" alt="error" /> 
                        @error('email')
                            {{ $message }}
                        @enderror
                    </div>
                </div>
                <div class="clear"></div>
                <div class="col-md-6 form-box @error('name') error-box @enderror">
                    <label class="req">First Name</label>
                    <input type="text" class="form-field"  name="name" value="{{ old('name') }}">
                    <div class="error"><img src="{{asset('img/error-arrow.png')}}" alt="error" />
                        @error('name')
                            {{ $message }}
                        @enderror
                    </div>
                </div>
                <div class="col-md-6 form-box @error('lname') error-box @enderror">
                    <label class="req">Last name</label>
                    <input type="text" class="form-field"  name="lname" value="{{ old('lname') }}">
                    <div class="error"><img src="{{asset('img/error-arrow.png')}}" alt="error" />
                        @error('lname')
                            {{ $message }}
                        @enderror
                    </div>
                </div>
                <div class="col-md-6 form-box @error('organization') error-box @enderror">
                    <label class="req">Organization</label>
                    <input type="text" class="form-field"  name="organization" value="{{ old('organization') }}">
                    <div class="error"><img src="{{asset('img/error-arrow.png')}}" alt="error" />
                        @error('organization')
                            {{ $message }}
                        @enderror
                    </div>
                </div>
                @php
                    $organizationtypes = App\Organizationtype::where(['status' => 1])->get();
                    $sources = App\Source::where(['status' => 1])->orderBy('sr_no')->get();
                @endphp
                <div class="col-md-6 form-box @error('organization_id') error-box @enderror">
                    <label class="req">Organization Type</label>
                    <select class="select2 form-field select2-clr" name="organization_id" >
                        <option></option>
                        @foreach( $organizationtypes as $organizationtype )
                            <option value="{{$organizationtype->id}}">{{$organizationtype->type}}</option>
                        @endforeach
                    </select>
                    <div class="error"><img src="{{asset('img/error-arrow.png')}}" alt="error" />
                        @error('organization_id')
                            {{ $message }}
                        @enderror
                    </div>
                </div>
                <div class="col-md-6 form-box @error('job_title') error-box @enderror">
                    <label class="req">Job Title</label>
                    <input type="text" class="form-field"  name="job_title">
                    <div class="error"><img src="{{asset('img/error-arrow.png')}}" alt="error" value="{{ old('job_title') }}"/>
                        @error('job_title')
                            {{ $message }}
                        @enderror
                    </div>
                </div>
                <div class="col-md-6 form-box @error('contact') error-box @enderror">
                    <label>Phone Number</label>
                    <input type="number" class="form-field" name="contact">
                    <div class="error"><img src="{{asset('img/error-arrow.png')}}" alt="error" value="{{ old('contact') }}"/>
                        @error('contact')
                            {{ $message }}
                        @enderror
                    </div>
                </div>
                <div class="col-md-6 form-box @error('address') error-box @enderror">
                    <label class="req">Address</label>
                    <input type="text" class="form-field"  name="address">
                    <div class="error"><img src="{{asset('img/error-arrow.png')}}" alt="error" value="{{ old('address') }}"/>
                        @error('address')
                            {{ $message }}
                        @enderror
                    </div>
                </div>
                <div class="col-md-6 form-box @error('salix_manager_name') error-box @enderror">
                    <label>Name of your Salix Account Manager</label>
                    <input type="text" class="form-field" name="salix_manager_name">
                    <div class="error"><img src="{{asset('img/error-arrow.png')}}" alt="error" />
                        @error('salix_manager_name')
                            {{ $message }}
                        @enderror
                    </div>
                </div>
                <div class="col-md-6 form-box @error('source_id') error-box @enderror">
                    <label class="req">How did you hear about us?</label>
                    <select class="select2 form-field select2-clr" name="source_id" onchange='checkAbout(this.value);'>
                        <option></option>
                        @foreach( $sources as $source )
                            <option value="{{$source->id}}">{{$source->sources}}</option>
                        @endforeach
                    </select>
                    <div class="error"><img src="{{asset('img/error-arrow.png')}}" alt="error" />
                        @error('source_id')
                            {{ $message }}
                        @enderror
                    </div>
                </div>
                <div class="col-md-6 form-box @error('code') error-box @enderror" id="refcode"
                @if ($errors->first('code'))
                @else
                style="display:none;"
                @endif
                >
                    <label class="req">Add Code Here</label>
                    <input type="text" class="form-field" name="code">
                    <div class="error"><img src="{{asset('img/error-arrow.png')}}" alt="error" />
                        @error('code')
                            {{ $message }}
                        @enderror
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="check-box">
                        <input type="checkbox" id="chk1" name="update">
                        <label for="chk1">I agree to receive periodic updates and communications via
                            email from this
                            website.</label>
                    </div>
                    <div class="check-box  @error('agreement_check') error-box @enderror">
                        <input type="checkbox" id="chk2" name="agreement_check">
                        <label for="chk2" class="req">I accept the
                            <a href="#" data-toggle="modal" data-target="#regModal">License Agreement.</a>
                        </label>
                        <div class="error"><img src="{{asset('img/error-arrow.png')}}" alt="error" />
                            @error('agreement_check')
                                {{ $message }}
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="g-recaptcha" data-sitekey="6Lfo6IYcAAAAAC9cYJWuvS8JQIjCpXB6uWcuYNTr"></div>
                    <div class="error"
                    style="display: block;"
                    >
                        @if($errors->has('g-recaptcha-response'))
                        <img src="{{asset('img/error-arrow.png')}}" alt="error" />
                            {{ $errors->first('g-recaptcha-response') }}
                        @endif
                    </div>
                </div>
                <div class="col-md-12 form-box">
                    <button >Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
{{-- <div class="contact-msg">
    <div class="default-box ">
        <h2>Thank you for applying to become a member of ACE!</h2>
        <p>You should expect an email within the next 48 hours with information about how to access the
            materials on this site.</p>
    </div>
</div> --}}
@include('partials.agreement')
{{-- Ends Here --}}
@endsection

@section('extra-js')
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script type="text/javascript">

function checkAbout(val){
 var element=document.getElementById('refcode');
 if(val=='6')
   element.style.display='block';
 else  
   element.style.display='none';
}

</script> 
@endsection
