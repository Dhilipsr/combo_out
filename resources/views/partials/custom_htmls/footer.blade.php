<footer>
<div class="container">
    <div class="footImg">
                            <div class="copyBox">
                                <div class="copyWid">
                                    
                                    <div class="copyrig">
                                        <p>© 2020 Salix Pharmaceuticals or its affiliates.</p>
                                        <p>{{$healthTool->item_code}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="ftImg2">
                                <img src="{{asset('custom_htmls/img/liver-health-logo.jpg')}}" alt="Logo">
                            </div>
                        </div>
    <!--<div class="footImg">-->
    <!--    <div class="ftImg1">-->
    <!--        <img src="{{asset('custom_htmls/img/salix-logo.png')}}" alt="Logo" />-->
    <!--    </div>-->
    <!--    <div class="ftImg2">-->
    <!--        <img src="{{asset('custom_htmls/img/liver-health-logo.jpg')}}" alt="Logo">-->
    <!--    </div>-->
    <!--</div>-->

    <!--<div class="copyBox">-->
    <!--    <div class="copyWid">-->
    <!--        <p>Salix Pharmaceuticals: 400 Somerset Corporate Blvd., Bridgewater, NJ 08807 </p>-->
    <!--        <div class="copyrig">-->
    <!--            <p>© 2020 Salix Pharmaceuticals or its affiliates.</p>-->
    <!--            <p>{{$healthTool->item_code}}</p>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--</div>-->
</div>
</footer>