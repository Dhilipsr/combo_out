<!doctype html>

<html lang="en" ng-app="">

    <!--<![endif]-->



  <!doctype html>

<html class="no-js" lang="">



<head>

    <meta charset="utf-8">

    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>LHN - @yield('title')</title>

    <meta name="description" content="">
    <meta name="insight-app-sec-validation" content="28d4287b-6319-4724-b568-e62e97919fbe">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="manifest" href="site.webmanifest">

    <link rel="apple-touch-icon" href="{{asset('icon.png')}}">


    <link rel="icon" href="{{asset('icon.png')}}" type="image/gif" sizes="16x16">


    <!-- Place favicon.ico in the root directory -->

    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">

    <link rel="stylesheet" href="{{asset('css/normalize.css')}}">

    <link rel="stylesheet" href="{{asset('css/main.css')}}">

    <link rel="stylesheet" href="{{asset('css/responsive.css')}}">

    <link rel="stylesheet" href="{{asset('css/swiper.min.css')}}">

    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-160643492-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-160643492-1');
</script>

<!-- Google Analytics -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-160643492-1', 'auto');
ga('send', 'pageview');
</script>
<!-- End Google Analytics -->

<!-- Google Tag Manager -->
<!--<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':-->
<!--new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],-->
<!--j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=-->
<!--'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);-->
<!--})(window,document,'script','dataLayer','GTM-KQJ76HN');</script>-->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P8MQFWR');</script>
<!-- End Google Tag Manager -->
</head>





    <body>
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P8MQFWR"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        {{-- header starts here --}}

        @include('partials.header')

        {{-- header ends here --}}



        <!-- Main Container Starts -->

        <div class="mainContainer">

            @yield('content')

        </div>

        <!-- Main Container Ends -->


        <div class="footer-note">
        <p>Content contained in this educational disease-state resource is being provided by Salix Pharmaceuticals for
        informational purposes only.
        Physicians should use their own clinical judgment in diagnosing, counseling, and advising patients.</p>
        </div>

        {{-- footer starts here --}}

        @include('partials.footer')

        {{-- footer ends here --}}
        
        
        <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KQJ76HN"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

        <!-- jQuery -->

         <script src="{{asset('js/vendor/modernizr-3.5.0.min.js')}}"></script>

        <script src="{{asset('js/vendor/jquery-3.2.1.min.js')}}"></script>

        <script src="{{asset('js/bootstrap.js')}}"></script>

        <script src="{{asset('js/plugins.js')}}"></script>

        <script src="{{asset('js/main.js')}}"></script>
        <script>
        $(document).ready(function(){

         fetch_customer_data();

         function fetch_customer_data(query = '')
         {
          $.ajax({
           url:"{{ route('live_search.action') }}",
           method:'GET',
           data:{query:query},
           dataType:'json',
           success:function(data)
           {
             console.log(data)
            $('#healthtools_ajax_results').html(data.table_data);
            $('#ebw_ajax_results').html(data.table_data_two);
            $('#guidelines_ajax_results').html(data.table_data_three);
            $('#total_records').text(data.total_data);
           }
          })
         }

         $(document).on('keyup', '#search', function(){
          var query = $(this).val();
          console.log(query);
          fetch_customer_data(query);
         });
        });
        </script>
        

        @yield('extra-js')

    </body>



</html>