<!doctype html>
<html lang="en">
    <!--<![endif]-->

    <head>
        <meta charset="utf-8">
        <meta name="insight-app-sec-validation" content="fe79377d-e0a1-402e-b1ef-4107704fe9b8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>LHN - @yield('title')</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="{{asset('icon.png')}}" type="image/x-icon">
        <link rel="icon" href="{{asset('icon.png')}}" type="image/gif" sizes="16x16">

        <link rel="stylesheet" href="{{asset('custom_htmls/css/main.css')}}"> <!-- main -->
        
         <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-160643492-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-160643492-1');
</script>

<!-- Google Analytics -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-160643492-1', 'auto');
ga('send', 'pageview');
</script>
<!-- End Google Analytics -->

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KQJ76HN');</script>
<!-- End Google Tag Manager -->
    </head>

    <body>
        <div class="mainContainer">
            <div class="mainContainerIn">

                @yield('content')
                
                <!-- Footer Starts -->
                @include('partials.custom_htmls.footer')
                <!-- Footer Ends -->

            </div>
        </div>

        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="{{asset('custom_htmls/js/common.js')}}"></script>

    </body>

</html>
