@section('title')
Home
@endsection
@extends('layouts.layout')
@section('content')
<!-- Start Here -->
<div class="banner-container">
    <div class="banner-container__top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>HE Guidelines and Information</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section eved-start">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-10 col-12">
                @foreach( $guidelines_page_contents as $guidelines_page_content )
                <h3>{!! $guidelines_page_content->content !!}</h3>
                @endforeach
            </div>
            <div class="col-12">
                <div class="guideline-links">
                    <ul>
                        @foreach( $plugins as $plugin )
                            <li>
                                <a href="#" data-toggle="modal"  class="click_check" data-target="#guideModal_{{$plugin->id}}" data-url={{$plugin->preety_link}} >
                                    @if($plugin->type == 0)
                                        <img src="{{asset('img/play.png')}}" alt="img" />
                                    @endif
                                    {{$plugin->title}}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Ends Here -->
<!-- Modal -->
@foreach( $plugins as $plugin )
    <div id="guideModal_{{$plugin->id}}" class="modal fade" role="dialog">
        <div class="modal-dialog modal-box">
            <div class="model-close" data-dismiss="modal"><img src="{{asset('img/close-white.png')}}" alt="img" /></div>
            <div class="row guidePop">
                <div class="col-md-12">
                    <h3>Thank you for visiting the ACE website;<br> you are now leaving our site</h3>
                    <ul>
                        <li><a href="{{route('guidelines')}}"><img src="{{asset('img/arrow-white-left.png')}}" alt="img" /> &nbsp;&nbsp; Go Back</a>
                        </li>
                        <li><a href="{{$plugin->preety_link}}" target="_blank">Continue &nbsp;&nbsp; <img src="{{asset('img/arrow-white-right.png')}}" alt="img" /></a>
                        </li>
                    </ul>
                    <p class="text-center text-white">The website you are about to visit is not affiliated with Salix Pharmaceutical or its affiliated entities, and is not responsible for the content, format, maintenance,or policies of the website you are about to visit. Salix Pharmaceutical or its affiliated entities do not monitor non-affiliated websites for accuracy. This link does not imply endorsement or support of any program, products, or services associated with the website.</p>
                </div>
                <div class="col-md-12 text-center">
                    <div class="check-box">
                        <input type="checkbox" class="do-not-show"  id="chk_{{$plugin->id}}">
                        <label for="chk_{{$plugin->id}}">Don’t show me this message again.</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach
@endsection

@section('extra-js')
<script type="text/javascript">
  $(document).ready(function(){
    $('.click_check').click(function(e){
        var local_check = localStorage.getItem("donotshow");
          if (local_check == 1) {
            var link = $(this).attr('data-url');
            window.location.href = link;
             e.stopPropagation();
          }else{

          }
    });
    $(".do-not-show").click(function(){
    
      if($(this). prop("checked") == true) {
        localStorage.setItem("donotshow", "1");
      }else{
        localStorage.removeItem("donotshow"); 
        
      }
    });
  });
</script>
@endsection