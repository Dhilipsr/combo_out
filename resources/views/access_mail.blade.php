<html>
    <head></head>
    <body>
<table width="650" style="margin: auto;">
        <tr>
            <td style="font:  15px/1.5 arial;">
                <p>
                    Welcome to LIVERHEALTHNOW! We are pleased to inform you that your LIVERHEALTHNOW account has been
                    approved. You now
                    have access to all the tools and resources in LIVERHEALTHNOW. Please log in at <a
                        href="http://liverhealthnow.com/" target="_blank" style="color: #2B79AC;">LIVERHEALTHNOW.com</a> to view them.</p>
                <p>Please do not reply to this message. Replies to this message are routed to an unmonitored mailbox. If you have questions, please contact us at: <a href="mailto:support@liverhealthnow.com" target="_blank" style="color: #2B79AC;">support@LIVERHEALTHNOW.com</a></p>
            </td>
        </tr>
        <tr>
            <td style="font:300 14px/1.5 arial" height="25px">

            </td>
        </tr>
        <tr>
            <td style="font:  12px/1.5 arial;">
                <p>Content contained in <a href="http://liverhealthnow.com/" target="_blank" style="color: #2B79AC;">LIVERHEALTHNOW.com</a> is being provided by Salix Pharmaceuticals for informational purposes
                    only.
                    Physicians should use their own clinical judgment in diagnosing, counseling, and advising patients.
                </p>
            </td>
        </tr>
        <tr>
            <td style="font:300 14px/1.5 arial" height="25px">

            </td>
        </tr>
        <tr>
            <td style="font: 12px/3 arial; text-align: center; ">
                <address style="font-style: normal;">Salix Pharmaceuticals: 400 Somerset Corporate Blvd.,
                    Bridgewater, NJ 08807.</address>
            </td>
        </tr>
        <tr>
            <td style="font:300 14px/1.5 arial" height="25px">

            </td>
        </tr>
        <tr>
            <td style="font:300  14px/1.5 arial; text-align: center;">
                <p><span><a
                        href="https://www.bauschhealth.com/privacy" target="_blank" style="color: #2B79AC;">PRIVACY POLICY</a></span> | <span
                        ><a
                        href="https://www.bauschhealth.com/terms" target="_blank" style="color: #2B79AC;">LEGAL NOTICE</a></span></p>
                <p>Please do not reply to this email. If you have questions related to LIVERHEALTHNOW’S Resources,
                    contact the LIVERHEALTHNOW support team at: <a href="mailto:support@liverhealthnow.com"
                        target="_blank" style="color: #2B79AC;">support@LIVERHEALTHNOW.com</a></p>

            </td>
        </tr>
        <tr>
            <td height="20px">

            </td>
        </tr>
        <tr>
            <td style="font:300  14px/1.5 arial; text-align: right;">
                <p>HED.0045.USA.20</p>

            </td>
        </tr>
    </table>

        <!-- <p>{{ date('d/m/Y',strtotime(now())) }}</p>
        <p>Welcome to LIVERHEALTHNOW! We are pleased to inform you that your LIVERHEALTHNOW account has been approved. You now have access to all the tools and resources in LIVERHEALTHNOW. Please log in at LIVERHEALTHNOW.com to view them.</p>
<p>Please do not reply to this email. If you have questions related to LIVERHEALTHNOW’S Resources,
contact the LIVERHEALTHNOW support team at: support@LIVERHEALTHNOW.com</p>

<p>Content contained in LIVERHEALTHNOW.com is being provided by Salix Pharmaceuticals for informational purposes only. Physicians should use their own clinical judgment in diagnosing, counseling, and advising patients.</p>

<address>Salix Pharmaceuticals: 400 Somerset Corporate Blvd.,
Bridgewater, NJ 08807.</address>

<p>PRIVACY POLICY | LEGAL NOTICE</p>
<p>Please do not reply to this email. If you have questions related to LIVERHEALTHNOW’S Resources,
contact the LIVERHEALTHNOW support team at: support@LIVERHEALTHNOW.com</p>

HED.0045.USA.20 -->

    </body>
</html>