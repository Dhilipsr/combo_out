@section('title')
Register
@endsection
@extends('layouts.layout')
@section('content')
<!-- Start Here -->
<div class="banner-container">
    <div class="banner-container__top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Registration </h2>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="contact-msg">
    <div class="default-box ">
        <h2>Thank you for applying to become a member of LIVERHEALTHNOW!</h2>
        <p>You should expect an email within the next 48 hours with information about how to access the
            materials on this site.</p>
    </div>
</div>
@include('partials.agreement')
{{-- Ends Here --}}
@endsection
